<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->getConnection()
    ->addColumn(
        $installer->getTable('newsletter/subscriber'),'subscriber_name',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable' => false,
            'length' => 50,
            'comment' => 'subscriber name',
            'primary' => false
        )
    );

