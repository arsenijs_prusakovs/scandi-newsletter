<?php

class Scandi_Newsletter_Model_Subscribe extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('newsletter/subscribe');
    }
}
