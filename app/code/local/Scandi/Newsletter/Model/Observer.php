<?php

class Scandi_Newsletter_Model_Observer extends Varien_Event_Observer
{
    /**
     * @param Varien_Event_Observer $observer
     */
    public function newsletterSubscriberSaveBefore(Varien_Event_Observer $observer)
    {
        $subscriber = $observer->getEvent()->getSubscriber();
        $name = Mage::app()->getRequest()->getParam('name');
        $subscriber->setSubscriberName($name);
    }

    /**
     * @param Varien_Event_Observer $observer
     * @return Scandi_Newsletter_Model_Observer
     */
    public function appendCustomColumn(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();
        if (!isset($block)) {
            return $this;
        }

        if ($block->getType() == 'adminhtml/newsletter_subscriber_grid') {
            /* @var $block Mage_Adminhtml_Block_Newsletter_Subscriber_Grid */
            $block->addColumnAfter('name', array(
                'header'    => 'Name',
                'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
                'index'     => 'subscriber_name',
            ), 'email');
        }
    }
}